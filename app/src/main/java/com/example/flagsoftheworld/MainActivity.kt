package com.example.flagsoftheworld

import android.graphics.Bitmap
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_flags.*

class MainActivity : AppCompatActivity() {

    private val COUNTRIES = listOf<String>("Australia",
            "Brazil",
            "China",
            "Egypt",
            "France",
            "Germany",
            "Ghana",
            "Italy",
            "Japan",
            "Mexico",
            "Nigeria",
            "Russia",
            "Spain",
            "United Kingdom",
            "United States")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flags)

        for (country in COUNTRIES) {
            createFlag(country)
        }

    }

    private fun createFlag(country: String) {
        val flag = layoutInflater.inflate(R.layout.country, null)
        val countryFlag = flag.findViewById<ImageView>(R.id.country_flag)
        val countryName = flag.findViewById<TextView>(R.id.country_name)

        val imageId = resources.getIdentifier(country.toLowerCase().replace(" ", ""), "drawable", packageName)
        countryFlag.setImageResource(imageId)
        countryFlag.setOnClickListener{
            showCountryInfo(country)
        }

        countryName.text = country
        grid_of_flags.addView(flag)
        val params = flag.layoutParams as ViewGroup.LayoutParams
        params.width = resources.displayMetrics.widthPixels/3
    }

    private fun showCountryInfo(country: String) {

        val country_tmp = country.toLowerCase().replace(" ", "")
        val anthemId = resources.getIdentifier(country_tmp + "_anthem", "raw", packageName)

        val mp = MediaPlayer.create(this, anthemId)
        mp.start()

        val textFileId = resources.getIdentifier(country_tmp, "raw", packageName)
        val fileText = resources.openRawResource(textFileId).bufferedReader().readText()

        val builder = AlertDialog.Builder(this)
        builder.setTitle(country)
        builder.setMessage(fileText)
        builder.setPositiveButton("OK") { _, _ ->
            mp.stop()
        }
        val dialog = builder.create()
        dialog.show()
    }
}